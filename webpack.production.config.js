'use strict';

const webpack = require('webpack');
const path = require('path');
const rules = require('./webpack.rules');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');

const extractCSS = new ExtractTextPlugin('stylesheets/style.css');

rules.push({
  test: /\.s?css/i,
  use: extractCSS.extract([
    {loader: 'css-loader', options: {minimize: true}},
    {loader: 'postcss-loader'},
    {loader: 'sass-loader'}
  ])
});

module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    publicPath: '/',
    path: path.join(__dirname, 'dist'),
    filename: '[chunkhash].js'
  },
  module: {
    rules
  },
  plugins: [
    new WebpackCleanupPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        drop_console: true,
        drop_debugger: true
      }
    }),
    extractCSS,
    new HtmlWebpackPlugin({
      template: './src/template.html',
      title: 'Bookshelf'
    })
  ]
};