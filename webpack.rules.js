module.exports = [
  {
    test: /\.js$/,
    exclude: /(node_modules|bower_components)/,
    use: 'babel-loader'
  },
  {
    test: /\.(svg|gif|jpg|png|woff|woff2|ttf|eot)/,
    use: 'url-loader'
  }
];