'use strict';

const webpack = require('webpack');
const path = require('path');
const rules = require('./webpack.rules');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const DashboardPlugin = require('webpack-dashboard/plugin');

const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || '1212';

rules.push(
  {
    test: /\.s?css$/,
    use: [
      {loader: 'style-loader', options: {sourceMap: true}},
      {loader: 'css-loader', options: {sourceMap: true}},
      {loader: 'postcss-loader', options: {sourceMap: true}},
      {loader: 'sass-loader', options: {sourceMap: true}}
    ]
  }
);

const config = {
  entry: [
    './src/index.js'
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules
  },
  devtool: process.env.WEBPACK_DEVTOOL || 'eval-source-map',
  devServer: {
    contentBase: './dist',
    hot: true,
    inline: true,
    historyApiFallback: true,
    port: PORT,
    host: HOST
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new DashboardPlugin(),
    new HtmlWebpackPlugin({
      template: './src/template.html'
    })
  ]
}

module.exports = config;